﻿using DroneDeliveryService.Models;
using DroneDeliveryService.Service;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DroneDeliveryService
{
    class Program
    {
        const int firstTrip = 1;
        static void Main(string[] args)
        {
            Console.WriteLine("Welcome, this is The Drone Delivery Service software v1");
            Console.WriteLine("Would you like to test it with MockData?");

            var droneMockService = new DroneService();
            var deliveryMockService = new DeliveryService();
            var tripService = new TripService();

            var Deliveries = deliveryMockService.CreateAllDeliveriesMocked();
            var Drones = droneMockService.CreateAllDronesMocked();

            ///remove from deliveries, overweight packages
            var overweightPackages = PackagesOverweightToDelivery(Drones, Deliveries);
            foreach (var item in overweightPackages)
            {
                Deliveries.Remove(item);
            }

            tripService.HandlingTripShippinments(Drones, Deliveries, firstTrip);

            ShowDronesReadyToGo(Drones);

            Console.ReadKey();

        }

        static List<Delivery> PackagesOverweightToDelivery(List<Drone> drones, List<Delivery> deliveries)
        {
            ////discover if there a package heavier than drones capacity
            var deliveriesOverweight = new List<Delivery>();
            foreach (var delivery in deliveries)
            {
                if (drones.All(x => x.MaximumWeight < delivery.PackageWeight))
                    deliveriesOverweight.Add(delivery);
            }

            if (deliveriesOverweight.Any())
            {
                Console.WriteLine("\n\n--------------------------------------------*-*-*-*-*-*--------------------------------------------");
                Console.WriteLine("\nWARNING! There are some deliveries that are impossible to deliver, because there's no drone able to deliver it!");
                Console.WriteLine("----*-*- Take a look: -*-*----");
                foreach (var item in deliveriesOverweight)
                {
                    Console.WriteLine($"The Package destinated to: '{item.Location}, weight: {item.PackageWeight}' is too heavy for any drone.");
                }

            }
            return deliveriesOverweight;
        }

        static void ShowDronesReadyToGo(List<Drone> drones)
        {
            Console.WriteLine("\n\n--------------------------------------------*-*-*-*-*-*--------------------------------------------");
            Console.WriteLine("These Drones are ready to go!\n\n");
            foreach (var drone in drones)
            {
                Console.WriteLine($"\nDrone: {drone.Name}");

                foreach (var trip in drone.Trips)
                {
                    if (trip.Deliveries.Any())
                    {
                        Console.WriteLine($"\nTrip: {trip.TripNumber}");
                        
                        foreach (var delivery in trip.Deliveries)
                        {
                            Console.WriteLine($"Location: {delivery.Location}");
                        }
                    }
                }
            
                Console.WriteLine("\n------------------------------------");
                
            }
        }
    }
}
