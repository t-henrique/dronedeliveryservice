﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DroneDeliveryService.Models
{
    public class Drone
    {
        public string Name { get; set; }
        public double MaximumWeight { get; set; }
        /////public List<Delivery> Deliveries { get; set; }

        public List<Trip> Trips { get; set; }

        public Drone()
        {
            Trips = new List<Trip>();
        }
    }
}
