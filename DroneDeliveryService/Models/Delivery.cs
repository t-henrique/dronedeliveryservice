﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DroneDeliveryService.Models
{
    public class Delivery
    {
        public string Location { get; set; }
        public double PackageWeight { get; set; }
    }
}
