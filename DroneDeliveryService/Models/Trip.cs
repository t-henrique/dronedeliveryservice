﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DroneDeliveryService.Models
{
    public class Trip
    {
        public int TripNumber { get; set; } = 1;
        public bool Full { get; set; } = false;
        public List<Delivery> Deliveries { get; set; }

        public Trip(int trip)
        {
            TripNumber = trip;
            Deliveries = new List<Delivery>();
        }

        public Trip()
        {
            Deliveries = new List<Delivery>();
        }
    }
}
