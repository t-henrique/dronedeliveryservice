﻿using DroneDeliveryService.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace DroneDeliveryService.Service
{
    public class DroneService
    {
        public List<Drone> CreateAllDronesMocked()
        {
            return new List<Drone>()
            {
                new Drone(){Name = "drone_a", MaximumWeight = 1000 },
                new Drone(){Name = "drone_b", MaximumWeight = 10000},
                new Drone(){Name = "drone_c", MaximumWeight = 5000},
                new Drone(){Name = "drone_d", MaximumWeight = 2000},
                new Drone(){Name = "drone_e", MaximumWeight = 4000},
            };
        }
    }
}
