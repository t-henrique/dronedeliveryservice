﻿using DroneDeliveryService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DroneDeliveryService.Service
{
    public class TripService
    {
        public void HandlingTripShippinments(List<Drone> drones, List<Delivery> packages, int tripRound)
        {
            foreach (var drone in drones)
            {
                drone.Trips.Add(new Trip(tripRound));
                ChargingDrone(drone, packages, tripRound);
            }
            if (packages.Any())
            {
                HandlingTripShippinments(drones, packages, tripRound + 1);
            }

        }

        public void ChargingDrone(Drone drone, List<Delivery> packages, int tripRound)
        {
            if (!drone.Trips.FirstOrDefault(x => x.TripNumber == tripRound).Full && packages.Any())
            {
                Delivery packageDisposed;

                FullfillingDrone(drone, packages, tripRound, out packageDisposed);

                packages.Remove(packageDisposed);

                ChargingDrone(drone, packages, tripRound);
            }
        }


        static void FullfillingDrone(Drone drone, List<Delivery> packages, int tripRound, out Delivery package)
        {
            var tripWeight = drone.Trips.FirstOrDefault(t => t.TripNumber == tripRound).Deliveries.Sum(x => x.PackageWeight);

            var availableWeight = drone.MaximumWeight - tripWeight;

            if (!packages.Any(x => x.PackageWeight <= availableWeight))
            {
                drone.Trips.FirstOrDefault(t => t.TripNumber == tripRound).Full = true;
                package = new Delivery();
            }
            else
            {
                package = packages.FirstOrDefault(x => x.PackageWeight <= availableWeight);
                drone.Trips.FirstOrDefault(x => x.TripNumber == tripRound).Deliveries.Add(package);
            }

        }

    }
}
