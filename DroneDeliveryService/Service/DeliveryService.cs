﻿using DroneDeliveryService.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace DroneDeliveryService.Service
{
    public class DeliveryService
    {
        public List<Delivery> CreateAllDeliveriesMocked()
        {
            return new List<Delivery>()
            {
                new Delivery(){ Location = "location_1", PackageWeight = 400},
                new Delivery(){ Location = "location_2", PackageWeight = 1500},
                new Delivery(){ Location = "location_3", PackageWeight = 200},
                new Delivery(){ Location = "location_4", PackageWeight = 2000},
                new Delivery(){ Location = "location_5", PackageWeight = 1800},
                new Delivery(){ Location = "location_6", PackageWeight = 5000},
                new Delivery(){ Location = "location_7", PackageWeight = 20000},
                new Delivery(){ Location = "location_8", PackageWeight = 10001},
                new Delivery(){ Location = "location_9", PackageWeight = 110000},
                new Delivery(){ Location = "location_10", PackageWeight = 95000},
                new Delivery(){ Location = "location_11", PackageWeight = 400},
                new Delivery(){ Location = "location_12", PackageWeight = 1500},
                new Delivery(){ Location = "location_13", PackageWeight = 200},
                new Delivery(){ Location = "location_14", PackageWeight = 2000},
                new Delivery(){ Location = "location_15", PackageWeight = 1800},
                new Delivery(){ Location = "location_16", PackageWeight = 5000},
                new Delivery(){ Location = "location_17", PackageWeight = 20000},
                new Delivery(){ Location = "location_18", PackageWeight = 10001},
                new Delivery(){ Location = "location_19", PackageWeight = 110000},
                new Delivery(){ Location = "location_20", PackageWeight = 95000},
                new Delivery(){ Location = "location_21", PackageWeight = 400},
                new Delivery(){ Location = "location_22", PackageWeight = 1500},
                new Delivery(){ Location = "location_23", PackageWeight = 200},
                new Delivery(){ Location = "location_24", PackageWeight = 2000},
                new Delivery(){ Location = "location_25", PackageWeight = 1800},
                new Delivery(){ Location = "location_26", PackageWeight = 5000},
                new Delivery(){ Location = "location_27", PackageWeight = 20000},
                new Delivery(){ Location = "location_28", PackageWeight = 10001},
                new Delivery(){ Location = "location_29", PackageWeight = 110000},
                new Delivery(){ Location = "location_30", PackageWeight = 95000},
                new Delivery(){ Location = "location_31", PackageWeight = 400},
                new Delivery(){ Location = "location_32", PackageWeight = 1500},
                new Delivery(){ Location = "location_33", PackageWeight = 200},
                new Delivery(){ Location = "location_34", PackageWeight = 2000},
                new Delivery(){ Location = "location_35", PackageWeight = 1800},
                new Delivery(){ Location = "location_36", PackageWeight = 5000},
                new Delivery(){ Location = "location_37", PackageWeight = 20000},
                new Delivery(){ Location = "location_38", PackageWeight = 10001},
                new Delivery(){ Location = "location_39", PackageWeight = 110000},
                new Delivery(){ Location = "location_40", PackageWeight = 95000},
                new Delivery(){ Location = "location_41", PackageWeight = 400},
                new Delivery(){ Location = "location_42", PackageWeight = 1500},
                new Delivery(){ Location = "location_43", PackageWeight = 200},
                new Delivery(){ Location = "location_44", PackageWeight = 2000},
                new Delivery(){ Location = "location_45", PackageWeight = 1800},
                new Delivery(){ Location = "location_46", PackageWeight = 20000},
                new Delivery(){ Location = "location_47", PackageWeight = 10001},
                new Delivery(){ Location = "location_48", PackageWeight = 5000},
                new Delivery(){ Location = "location_49", PackageWeight = 110000},
                new Delivery(){ Location = "location_60", PackageWeight = 95000},
                new Delivery(){ Location = "location_61", PackageWeight = 95000},
                new Delivery(){ Location = "location_620", PackageWeight = 95000},
                new Delivery(){ Location = "location_630", PackageWeight = 95000},
                new Delivery(){ Location = "location_650", PackageWeight = 95000},
                new Delivery(){ Location = "location_640", PackageWeight = 95000},
                new Delivery(){ Location = "location_65", PackageWeight = 95000},
                new Delivery(){ Location = "location_66", PackageWeight = 95000},
                new Delivery(){ Location = "location_67", PackageWeight = 95000},
                new Delivery(){ Location = "location_68", PackageWeight = 95000},
                new Delivery(){ Location = "location_69", PackageWeight = 95000},
                new Delivery(){ Location = "location_70", PackageWeight = 95000},
                new Delivery(){ Location = "location_711", PackageWeight = 95000},
            };
        }
    }
}
